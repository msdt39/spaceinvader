package controller;

/**
 * An Interface for all the Methods that are used inside the View.
 */
public interface IController {
    /**
     * A Method for switching through the different Screens.
     */
    void nextScreen();

    /**
     * A Method that takes the inputs from the User/Keyboard.
     * @param key The pressed key at the Keyboard.
     */
    void userInput(int key);

    /**
     * A Method for getting the Healthpoints for drawing them at the top of the GameScreen.
     * @return Healthpoints of the Spaceship.
     */
    int getHealthPoints();
    /**
     * A Method for getting the Score points for drawing them at the top of the GameScreen.
     * @return Reached score of the Player.
     */
    int getScore();
}
