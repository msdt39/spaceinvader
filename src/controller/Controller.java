package controller;

import model.Direction;
import model.GameModel;
import view.IView;

/**
 * The Controller Class is the part that connecting the Model and the View.
 */
public class Controller implements IController {
    /**
     * Creates a Model object.
     */
    private GameModel model;
    /**
     * Creates a View object.
     */
    //private Main main;
    private IView view;
    /**
     * The state of which window is running.
     */
    private State state;

    /**
     * The Constructor of the Controller Class.
     *
     * @param view   The taken View object.
     * @param width  The taken width of the applications window.
     * @param height The taken height of the applications window.
     */
    public Controller(IView view, int width, int height) {
        this.view = view;
        this.model = new GameModel(width, height);
        this.state = State.START_SCREEN;
    }

    /**
     * For setting the View at Controller
     * @param view The Interface of the View
     */
    public void setView(IView view) {
        this.view = view;
    }

    /**
     * A Method for choosing wich window is the next on Screen of the Application.
     * It decides which one of the four Screens are running.
     */
    public void nextScreen() {
        switch (state) {
            case START_SCREEN:
                view.drawStartscreen();
                break;
            case TUTORIAL_SCREEN:
                view.drawTutorialScreen();
                break;
            case GAME_SCREEN:
                view.drawBackground(this.model.getBackground().getPosY1(), this.model.getBackground().getPosY2());
                view.drawGameScreen();
                for (int i = 0; i < this.model.asteroids.length; i++) {
                    view.drawAsteroid(this.model.asteroids[i].respawnX(), this.model.asteroids[i].fly());
                    if (this.model.asteroids[i].getxPos() < 650) {
                        this.model.asteroids[i].setxPos(this.model.asteroids[i].getxPos() + 200);
                    }
                    if (this.model.asteroids[i].getxPos() > 50) {
                        this.model.asteroids[i].setxPos(this.model.asteroids[i].getxPos() - 200);
                    }
                    this.model.asteroids[0].setyPos(this.model.asteroids[0].getyPos() - 2);
                    this.model.asteroids[1].setyPos(this.model.asteroids[1].getyPos() + 1);
                    this.model.asteroids[2].setyPos(this.model.asteroids[2].getyPos() - 1);
                }

                view.drawGoldCoin(this.model.getCoin().respawnX(), this.model.getCoin().fly());
                view.drawSpaceShip((int) this.model.getShip().getxPos(), (int) this.model.getShip().getyPos());

                view.drawLaser((int) this.model.getShip().getxPos() + 30, this.model.laserFly());

                if (model.getFlame().getFlammeAnimation() == 0) {
                    view.drawFlame1((int) this.model.getShip().getxPos() + 9, (int) this.model.getShip().getyPos() + 90);
                } else if (model.getFlame().getFlammeAnimation() == 1) {
                    view.drawFlame2((int) this.model.getShip().getxPos() + 9, (int) this.model.getShip().getyPos() + 90);
                }
                if (model.getShip().getHealthPoints() <= 0) {
                    state = State.GAME_OVER_SCREEN;
                }
                break;
            case GAME_OVER_SCREEN:
                view.drawGameOverScreen();
                break;
        }
    }

    /**
     * This Method is taken the User inputs from the keyboard in the first two screens and the last one it's only about switching to the next Screen and in the GAME_SCREEN it's although taken the key codes for navigating the Spaceship.
     *
     * @param key the given keycode.
     */
    public void userInput(int key) {
        switch (state) {
            case START_SCREEN:
                if (key == 10) {
                    state = State.TUTORIAL_SCREEN;
                }
                break;
            case TUTORIAL_SCREEN:
                if (key == 82) {
                    state = State.GAME_SCREEN;
                    model.setScore(0);
                }
            case GAME_SCREEN:
                this.model.collision();
                if (key == 87) {
                    this.model.navigate(Direction.UP);
                } else if (key == 83) {
                    this.model.navigate(Direction.DOWN);
                } else if (key == 68) {
                    this.model.navigate(Direction.RIGHT);
                } else if (key == 65) {
                    this.model.navigate(Direction.LEFT);
                }
                if (key == 32) {
                    this.model.navigate(Direction.SHOOT);
                }
                if (model.getLoose() == true) {
                    state = State.GAME_OVER_SCREEN;
                }
                break;
            case GAME_OVER_SCREEN:
                model.setLoose(false);
                model.getShip().setHealthPoints(3);
                model.getShip().setyPos(model.getShip().getyStartPosition());
                model.getShip().setxPos(model.getShip().getxStartPosition());
                if (key == 10) {
                    state = State.GAME_SCREEN;
                    model.setScore(0);
                }
        }


    }

    /**
     * A Method for getting the health points of the Spaceship.
     * @return Health points.
     */
    public int getHealthPoints() {
        return model.getShip().getHealthPoints();
    }

    /**
     * A Method for getting the reached Score of the User
     * @return Score points
     */
    public int getScore() {
        return model.getScore();
    }

}
