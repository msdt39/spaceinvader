package controller;
/**
 * These are all possiable Game stats the game could reach. It'll be used in the Controller to implement the states.
 */
public enum State {
    /**
     * The first screen when starting the Application.
     */
    START_SCREEN,
    /**
     * The second screen is telling the user how to play.
     */
    TUTORIAL_SCREEN,
    /**
     *  Play the Game.
     */
    GAME_SCREEN,
    /**
     * After losing the Game and having the choice to restart.
     */
    GAME_OVER_SCREEN
}
