/**
 * Inside this Package the Controller is defined. The Controller is necessary for sending the user inputs from the View
 * to the Model and the calculated logics from the Model back to the View.
 */
package controller;