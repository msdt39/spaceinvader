/**
 * Inside The View Package is defined, how to draw the Game of the Devices,
 * it takes the User inputs and sends them to the Controller.
 */
package view;