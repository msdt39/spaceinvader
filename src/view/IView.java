package view;

/**
 * Inside this Interface are alle Methods written down, that are used at the Controller.
 */
public interface IView {
    /**
     * A method for drawing the Start Screen.
     */
    void drawStartscreen();
    /**
     * A method for drawing the Tutorial Screen.
     */
    void drawTutorialScreen();
    /**
     * A method for drawing the Game Screen.
     */
    void drawGameScreen();
    /**
     * A method for drawing the Game Over Screen.
     */
    void drawGameOverScreen();
    /**
     * A Method for Drawing the Background of the GameScreen.
     * @param posY1 The starting Position of the Background image 1.
     * @param posY2 The starting Position of the Background image 2.
     */
    void drawBackground(int posY1, int posY2);
    /**
     * A Method for Drawing the Asteroids.
     * @param posX The X-position of the Asteroid.
     * @param posY The X-position of the Asteroid.
     */
    void drawAsteroid(int posX, int posY);
    /**
     * A Method for Drawing the Coin.
     * @param posX The X-position of the Coin.
     * @param posY The Y-position of the Coin.
     */
    void drawGoldCoin(int posX, int posY);
    /**
     * A Method for Drawing the Spaceship.
     * @param xPos The X-position of the Spaceship.
     * @param yPos The Y-position of the Spaceship.
     */
    void drawSpaceShip(int xPos, int yPos);
    /**
     * A Method for Drawing the Laser beam.
     * @param posX The X-position of the Laser.
     * @param posY The Y-position of the Laser.
     */
    void drawLaser(int posX, int posY);
    /**
     * A Method for Drawing the Flame animation of the Spaceship's engine.
     * @param posX The X-position of the Flame.
     * @param posY The Y-position of the Flame.
     */
    void drawFlame1(int posX, int posY);
    /**
     * A Method for Drawing the Flame animation of the Spaceship's engine.
     * @param posX The X-position of the Flame.
     * @param posY The Y-position of the Flame.
     */
    void drawFlame2(int posX, int posY);
}
