package view;

import processing.core.PApplet;
import controller.Controller;

/**
 * The Main Class for running the whole Programm.
 */
public class Main {
    /**
     * The Main Method of the Programm.
     * The second part is for connecting the View and the Controller
     * The third part is for starting the Processing sketch
     * @param args An Array of String Arguments.
     */
    public static void main(String[] args) {
        var view = new ProcessingView();
        var controller = new Controller(view,750,1000);

        controller.setView(view);
        view.setController(controller);

        PApplet.runSketch(new String[]{"ProcessingView"}, view);
    }
}