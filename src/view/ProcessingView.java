package view;

import controller.Controller;
import controlP5.*;
import controller.IController;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * This Class is drawing the Processing screen.
 */
public class ProcessingView extends PApplet implements IView{
    /**
     * The Width of the Screen.
     */
    private final int width = 750;
    /**
     * The Height of the Screen.
     */
    private final int height = 1000;
    /**
     * The Background of the Start and the GameOver Screen.
     */
    private PImage backgroundStart;
    /**
     * The Background Image 1 of the GameScreen.
     */
    private PImage backgroundImage;
    /**
     * The Background Image 2 of the GameScreen.
     */
    private PImage background2Image;
    /**
     * The Image of the Spaceship.
     */
    private PImage shipImage;
    /**
     * The Image of the Asteroid.
     */
    private PImage asteroidImage;
    /**
     * The Image of the Coin.
     */
    private PImage goldCoin;
    /**
     * The Image of the Laser beam.
     */
    private PImage laser;
    /**
     * The Image of the Spaceship's flame.
     */
    private PImage flame1;
    /**
     * The Image of the Spaceship's flame 2.
     */
    private PImage flame2;
    /**
     * Object of ControlP5.
     */
    private ControlP5 cp5;
    /**
     * Object of the Controller.
     */
    IController controller;

    /**
     * The default settings() Method from Processing.
     */
    public void settings() {
        size(width, height);
    }
    /**
     * The default setup() Method from Processing.
     */
    public void setup() {
        this.controller = new Controller(this, width, height);
        backgroundStart = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/Background1.jpg");
        backgroundImage = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/Background5.1.png");
        background2Image = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/Background5.1.png");
        shipImage = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/SpaceShip2.png");
        asteroidImage = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/Asteroid1.2.png");
        goldCoin = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/CoinGold1.1.png");
        laser = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/Laser1.1.png");
        flame1 = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/Flamme1.1.png");
        flame2 = loadImage("/home/animus/IdeaProjects/SpaceInvaders2/pictures/Flamme1.2.png");

    }
    /**
     * The default draw() Method from Processing.
     */
    public void draw() {
        controller.nextScreen();
    }
    public void setController(IController controller) {
        this.controller = controller;
    }

    /**
     * In this Method the optic of the StartScreen and a Button for switching to the next Screen is defined.
     */
    public void drawStartscreen() {
        image(backgroundStart,0,0,750,1000);
        textSize(50);
        text("Welcome to Space Invaders \nAre you ready Captain?", 100, 200);
        textSize(40);
        fill(150,255,50);
        text("Press the Enter Button \nIf you are ready to save the Galaxy!", 100, 500);
        cp5 = new ControlP5(this);
        Button start = cp5.addButton("Start");
        start.setPosition(280,850);
        start.setSize(200,70);
        start.setColorLabel(255);
        cp5.mapKeyFor(new ControlKey() {
            @Override
            public void keyEvent() {
                controller.nextScreen();
                start.remove();
            }
        }, ControlP5Constants.ENTER);
//        start.addListenerFor(ControlP5Constants.ACTION_RELEASE, new CallbackListener() {
//            @Override
//            public void controlEvent(CallbackEvent callbackEvent) {
//                controller.setState(GAME_SCREEN);
//                start.remove();
//            }
//        });
    }

    /**
     * In this Method the optic of the TutorialScreen and a Button for switching to the next Screen is defined.
     */
    public void drawTutorialScreen() {
        image(backgroundStart,0,0,750,1000);
        textSize(30);
        text("Captain if you have forgotten how to fly your Spaceship, \nplease remember:\n" +
                "Press w for flying up\nPress a for flying to the left\nPress s for flying down\nPress d for flying to the right\n" +
                "Press the Space Button to fire a laser beam\n\n" +
                "And just in case you didn't know any more\nYou have 3 Lifepoints, \nloosing one everytime you hit an Asteroid\n" +
                "And collecting a gold coin gives you +50 to your Score\nDestroying one gives you +5 to your score\n" +
                "and now good luck, \nStart the game with the r Button", 10, 100);
        cp5 = new ControlP5(this);
        Button start2 = cp5.addButton("Ready for Takeoff!");
        start2.setPosition(280,850);
        start2.setSize(200,70);
        start2.setColorLabel(255);
        cp5.mapKeyFor(new ControlKey() {
            @Override
            public void keyEvent() {
                controller.nextScreen();
                start2.remove();
            }
        }, 82);
    }
    /**
     * In this Method the optic of the GameScreen is defined.
     */
    public void drawGameScreen() {
        fill(255,255,0);
        textSize(20);
        text("Healthpoints: ", 0,20);
        text(controller.getHealthPoints(),120,20);
        text("Score: ", 650, 20);
        text(controller.getScore(), 720, 20);
    }
    /**
     * In this Method the optic of the GameOver and a Button for switching back to the GameScreen is defined.
     */
    public void drawGameOverScreen() {
        image(backgroundStart,0,0,750,1000);
        textSize(50);
        text("GAME OVER \nYour Score is: " + controller.getScore(), 70,200);
        textSize(30);
        text("Want to restart? \nPress Enter\nWant to close the Game?\nPress the Esc key at your Keyboard", 70,500);
        cp5 = new ControlP5(this);
        Button restart = cp5.addButton("Restart");
        restart.setPosition(280,850);
        restart.setSize(200,70);
        restart.setColorLabel(255);
        cp5.mapKeyFor(new ControlKey() {
            @Override
            public void keyEvent() {
                controller.userInput(10);
                restart.remove();
            }
        }, ControlP5Constants.ENTER);
    }

    /**
     * Method for Drawing the pictures of the Background of the GameScreen.
     * @param posY1 Y-position of picture 1 from the GameScreen.
     * @param posY2 Y-position of picture 2 from the GameScreen.
     */
    public void drawBackground(int posY1, int posY2) {
        image(backgroundImage,0,posY1);
        image(background2Image,0,posY2);
    }

    /**
     * Method for drawing the Image of the Asteroid.
     * @param posX The X-coordinate of the Asteroid.
     * @param posY The Y-Coordinate of the Asteroid.
     */
    public void drawAsteroid(int posX, int posY) {
        image(asteroidImage, posX, posY);
    }
    /**
     * Method for drawing the Image of the GoldCoin.
     * @param posX The X-coordinate of the GoldCoin.
     * @param posY The Y-Coordinate of the GoldCoin.
     */
    public void drawGoldCoin(int posX, int posY) {
        image(goldCoin,posX, posY);
    }

    /**
     * Method for drawing the Image of the Spaceship.
     * @param xPos The X-coordinate of the Spaceship.
     * @param yPos The Y-coordinate of the Spaceship.
     */
    public void drawSpaceShip(int xPos, int yPos) {
        image(shipImage,xPos, yPos);
    }
    /**
     * Method for drawing the Image of the Laser.
     * @param posX The X-coordinate of the Laser.
     * @param posY The Y-Coordinate of the Laser.
     */
    public void drawLaser(int posX, int posY) {
        image(laser,posX,posY);
    }
    /**
     * Method for drawing the Image of the Flame1.
     * @param posX The X-coordinate of the Flame1.
     * @param posY The Y-Coordinate of the Flame1.
     */
    public void drawFlame1(int posX, int posY) {
        image(flame1, posX,posY);
    }
    /**
     * Method for drawing the Image of the Flame2.
     * @param posX The X-coordinate of the Flame2.
     * @param posY The Y-Coordinate of the Flame2.
     */
    public void drawFlame2(int posX, int posY) {
        image(flame2, posX, posY);
    }

    /**
     * The keyPressed() Method for reading the UserInputs from the Keyboard.
     */
    @Override
    public void keyPressed() {
        controller.userInput(keyCode);
    }
}
