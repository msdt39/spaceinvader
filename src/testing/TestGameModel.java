package testing;
import model.Direction;
import model.GameModel;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
/**
 * This Class is only for testing the logic from the Model.
 */
public class TestGameModel {
    /**
     * Testing if the ship is moving outside the boarders of the Game.
     */
    @Test
    void shipShoud_NotMoveOutsideTheBoarders() {
        var game = new GameModel(750,1000);
        game.getShip().setyPos(890);
        game.getShip().setxPos(740);
        game.navigate(Direction.DOWN);
        game.navigate(Direction.RIGHT);
        assertEquals(890, game.getShip().getyPos());
        assertEquals(740, game.getShip().getxPos());

        game.getShip().setyPos(10);
        game.getShip().setxPos(10);
        game.navigate(Direction.UP);
        game.navigate(Direction.LEFT);
        assertEquals(10, game.getShip().getyPos());
        assertEquals(10, game.getShip().getxPos());
    }
    /**
     * Testing if the ship is moving inside the boarders of the Game at the vertikal line.
     */
    @Test
    void ship_ShouldMoveInsideTheBoarders1() {
        var game = new GameModel(750,1000);
        game.getShip().setyPos(500);
        game.getShip().setxPos(200);
        game.navigate(Direction.DOWN);
        game.navigate(Direction.RIGHT);
        assertEquals(510, game.getShip().getyPos());
        assertEquals(217, game.getShip().getxPos());

    }
    /**
     * Testing if the ship is moving outside the boarders of the Game at the horizontal line.
     */
    @Test
    void ship_ShouldMoveInsideTheBoarders2() {
        var game = new GameModel(750,1000);
        game.getShip().setyPos(500);
        game.getShip().setxPos(200);
        game.getShip().setyPos(500);
        game.getShip().setxPos(200);
        game.navigate(Direction.UP);
        game.navigate(Direction.LEFT);
        assertEquals(480, game.getShip().getyPos());
        assertEquals(183, game.getShip().getxPos());
    }

    /**
     * Testing if the game recognizes if the user hast lost the round.
     */
    @Test
    void loose_ShouldBePossiable() {
        var game = new GameModel(750,1000);
        game.getShip().setHealthPoints(0);
        assertEquals(game.getLoose() == true, game.getLoose());
    }

    /**
     * Testing if the collision between the Spaceship and thew Asteroid is working.
     */
    @Test
    void coolisionWithAsteroid_ShouldBeRecognized() {
        var game = new GameModel(750,1000);
        game.getAsteroid1().setxPos(320);
        game.getAsteroid1().setyPos(-500);
        game.getShip().setxPos(320);
        game.getShip().setyPos(-500);
        game.collision();
        assertEquals(2, game.getShip().getHealthPoints());
    }
    /**
     * Testing if the collision between the Spaceship and thew Asteroid is not working if they are not colliding.
     */
    @Test
    void coolisionWithAsteroidShould_NotBeRecognized() {
        var game = new GameModel(750,1000);
        game.getAsteroid1().setxPos(320);
        game.getAsteroid1().setyPos(-500);
        game.getShip().setxPos(450);
        game.getShip().setyPos(-500);
        game.collision();
        assertEquals(3, game.getShip().getHealthPoints());
    }

    /**
     * Testing if the collision between the Spaceship and the Coin is working.
     */
    @Test
    void coolisionWithACoin_ShouldBeRecognized() {
        var game = new GameModel(750,1000);
        game.getCoin().setxPos(320);
        game.getCoin().setyPos(-500);
        game.getShip().setxPos(320);
        game.getShip().setyPos(-500);
        game.collision();
        assertEquals(70, game.getScore());
    }

    /**
     * Testing if the collision between the Spaceship and the Coin isn't working if they are not colliding.
     */
    @Test
    void coolisionWithACoinShould_NotBeRecognized() {
        var game = new GameModel(750,1000);
        game.getCoin().setxPos(320);
        game.getCoin().setyPos(-500);
        game.getShip().setxPos(430);
        game.getShip().setyPos(-500);
        game.collision();
        assertEquals(20, game.getScore());
    }

    /**
     * Tests if the Asteroid is moving from the top to the bottom of the game field.
     */
    @Test
    void Asteroid_IsFlying() {
        var game = new GameModel(750,1000);
        game.getAsteroid1().setyPos(-500);
        game.getAsteroid1().fly();
        assertEquals(-492, game.getAsteroid1().getyPos());
    }

    /**
     * Testing if the Asteroid is being reset if it reaches the boarder at the bottom from the game.
     */
    @Test
    void Asteroid_IsNotFlying() {
        var game = new GameModel(750,1000);
        game.getAsteroid1().setyPos(1200);
        game.getAsteroid1().fly();
        assertEquals(-200, game.getAsteroid1().getyPos());
    }

    /**
     * Testing if the Asteroid is respawning at inside the boarders of the game field.
     */
    @Test
    void Asteroid_IsRespawningX() {
        var game = new GameModel(750,1000);
        game.getAsteroid1().setyPos(1200);
        game.getAsteroid1().respawnX();
        assertTrue(game.getAsteroid1().getxPos() < 700 && game.getAsteroid1().getyPos() > 40);
    }
    /**
     * Testing if the Asteroid is not respawning outside the boarders of the game field.
     */
    @Test
    void Asteroid_IsNotRespawningX() {
        var game = new GameModel(750,1000);
        game.getAsteroid1().setyPos(1200);
        game.getAsteroid1().respawnX();
        assertFalse(game.getAsteroid1().getxPos() > 750 && game.getAsteroid1().getyPos() < 20);
    }

    /**
     * Testing if the Coin is moving from the top to the bottom of the game field.
     */
    @Test
    void Coin_IsFlying() {
        var game = new GameModel(750,1000);
        game.getCoin().setyPos(-500);
        game.getCoin().fly();
        assertEquals(-492, game.getCoin().getyPos());
    }

    /**
     * Testing if the Coin is respawning at the correct y-coordinate.
     */
    @Test
    void Coin_IsNotFlying() {
        var game = new GameModel(750,1000);
        game.getCoin().setyPos(1200);
        game.getCoin().fly();
        assertEquals(-300, game.getCoin().getyPos());
    }
    /**
     * Testing if the Coin is respawning at the correct x-coordinate.
     */
    @Test
    void Coin_IsRespawningX() {
        var game = new GameModel(750,1000);
        game.getCoin().setyPos(1200);
        game.getCoin().respawnX();
        assertTrue(game.getCoin().getxPos() < 700 && game.getCoin().getyPos() > 40);
    }
    /**
     * Testing if the Coin is not respawning outside the boarders of the game field.
     */
    @Test
    void Coin_IsNotRespawningX() {
        var game = new GameModel(750,1000);
        game.getCoin().setyPos(1200);
        game.getCoin().respawnX();
        assertFalse(game.getCoin().getxPos() > 750 && game.getCoin().getyPos() < 20);
    }

    /**
     * Tests if the laser beam is flying from the position from the spaceship to the top of the game field.
     */
    @Test
    void LaserFly_IsFlying() {
        var game = new GameModel(750,1000);
        game.getLaser().setyPos(-350);
        game.laserFly();
        assertEquals(-385, game.getLaser().getyPos());
    }

    /**
     * Is testing, if the Laser is outside the boarders and the health points are smaller than 1 that the laser isn't moving anymore.
     */
    @Test
    void LaserFly_IsNotFlying() {
        var game = new GameModel(750,1000);
        game.getLaser().setyPos(-350);
        game.getLaser().setHealthPoints(0);
        game.laserFly();
        assertEquals(-350, game.getLaser().getyPos());
    }
    /**
     * Tests if the Background1 from the GameScreen is moving.
     */
    @Test
    void background1IsMoving() {
        var game = new GameModel(750,1000);
        game.getBackground().getPosY1();
        game.getBackground().backgroundMove();
        assertEquals(2,game.getBackground().getPosY1());
    }
    /**
     * Tests if the Background2 from the GameScreen is moving.
     */
    @Test
    void background2IsMoving() {
        var game = new GameModel(750,1000);
        game.getBackground().getPosY2();
        game.getBackground().backgroundMove();
        assertEquals(-998,game.getBackground().getPosY2());
    }

}
