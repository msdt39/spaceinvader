package model;

/**
 * This is the Main Model Class that combines all Classes of the Game logic.
 */
public class GameModel{
    /**
     * The Width and Height of the applications screen.
     */
    final int width, height;
    /**
     * The score the User collects.
     */
    private int score = 0;
    /**
     * For checking collisions and if the Game is lost.
     */
    private boolean isColliding;
    /**
     * Tells if the game is lost.
     */
    private boolean loose = false;
    /**
     * Creates a Spaceship object.
     */
    final SpaceShip ship;
    /**
     * Creates four Asteroid objects.
     */
    final Asteroid asteroid1 = new Asteroid();
    final Asteroid asteroid2 = new Asteroid();
    final Asteroid asteroid3 = new Asteroid();
    final Asteroid asteroid4 = new Asteroid();
    /**
     * Saving the four Asteroids in an Array.
     */
    public Asteroid asteroids[] = {asteroid1, asteroid2, asteroid3, asteroid4};
    /**
     * Creates a Coin object.
     */
    final Coin coin = new Coin();
    /**
     * Creats a Laser object
     */
    final Laser laser = new Laser();
    /**
     * Creats a Flame object
     */
    final Flame flame = new Flame();
    /**
     * Creates a Background object.
     */
    final Background background;

    /**
     * The Constructor of the GameModel Class
     *
     * @param width  The Width of the applications screen.
     * @param height The Height of the applications screen.
     */
    public GameModel(int width, int height) {
        this.width = width;
        this.height = height;
        background = new Background();
        ship = new SpaceShip(320, 750, 3);}
    /**
     * A method to navigate the Spaceship its necessary for define the boarders of the play Screen and moving the Ship.
     *
     * @param direction takes the chosen direction.
     */
    public void navigate(Direction direction) {
        switch (direction) {
            case UP:
                if (ship.getyPos() > 20) {
                    ship.setyPos(ship.getyPos() - ship.getSpeedUp());}
                break;
            case DOWN:
                if (ship.getyPos() < 820) {
                    ship.setyPos(ship.getyPos() + ship.getSpeedDown());}
                break;
            case RIGHT:
                if (ship.getxPos() < 650) {
                    ship.setxPos(ship.getxPos() + ship.getSpeedHorizontal());}
                break;
            case LEFT:
                if (ship.getxPos() > 20) {
                    ship.setxPos(ship.getxPos() - ship.getSpeedHorizontal());}
                break;
            case SHOOT:
                laser.setyPos((int) ship.getyPos());
                laser.setxPos((int) ship.getxPos());}}

    /**
     * A method for checking if there is a Collision of the Spaceship and an Enemy or a Coin.
     */
    public void collision() {
        var t1 = new Speed("Thread 1");
        t1.run();}

    /**
     * A Thread for making the Game faster. Inside is the logic for the collisions.
     */
    class Speed extends Thread {
        /**
         * Variable for giving a name.
         */
        final String name;

        /**
         * Constructor of the sub Speed Class.
         * @param name gives a Name to the Thread.
         */
        public Speed(String name) {
            this.name = name;}

        /**
         * This is the default run() Method of a Thread.
         */
        @Override
        public void run() {
            for (int i = 0; i < asteroids.length; i++) {
                if (collision.collisions((int)ship.getxPos(), asteroids[i].getxPos(), (int)ship.getyPos(), asteroids[i].getyPos())) {
                    asteroids[i].setyPos(-150);
                    ship.setHealthPoints(ship.getHealthPoints() - 1);
                    isColliding = false;
                    if (ship.getHealthPoints() <= 0) {
                        loose = true;
                    }
                }
            }

            if(collision.collisions((int)ship.getxPos(), coin.getxPos(), (int)ship.getyPos(), coin.getyPos())) {
                coin.setyPos(-200);
                score+=50;
                isColliding = false;

            }

            for (int i = 0; i < asteroids.length; i++) {
                if (collision.collisions(laser.getxPos(), asteroids[i].getxPos(), laser.getyPos(), asteroids[i].getyPos())) {
                    asteroids[i].setAlive(false);
                    asteroids[i].setyPos(-150);
                    score +=5;
                    isColliding = false;
                }
            }
        }

    }

    /**
     * Lambda expression for the Collisions of the different Objects.
     */
    Collision collision = (int xPos1, int xPos2, int yPos1, int yPos2) ->
    {if (((xPos1 + 100) >= (xPos2)) && (xPos1 <= (xPos2 + 100)) && ((yPos1) >= (yPos2 - 100)) && (yPos1 <= (yPos2 + 100))) {
        isColliding = true;
    } return isColliding;};

    /**
     * An Interface for creating a Lambda expression.
     */
    interface Collision { boolean collisions(int xPos1, int xPos2, int yPos1, int yPos2);}
    /**
     * This Method is calculating the movement of the shoot of a Laser.
     * @return The updating Y-Position of the Laser
     */
    public int laserFly() {
        if (laser.getHealthPoints() > 0) {
            laser.setShoot(true);
            laser.setyPos(laser.getyPos() - laser.getSpeed());
            laser.setxPos((int) ship.getxPos());
        }return laser.getyPos();}

    /**
     * Method for setting the Score.
     * @param score to set.
     */
    public void setScore(int score) {this.score = score;}
    /**
     * A Method for getting the score of the User.
     *
     * @return the score points.
     */
    public int getScore() {return score;}
    /**
     * This Method is returning if the game is lost or not.
     *
     * @return the boolean if the game is lost.
     */
    public boolean getLoose() {return loose;}
    /**
     * A Method for setting if the Game is lost.
     *
     * @param loose setts the loose boolean.
     */
    public void setLoose(boolean loose) {this.loose = loose;}
    /**
     * Getter of a Spaceship Object,
     * @return Spaceship Object
     */
    public SpaceShip getShip() {return ship;}
    /**
     * Getter of a Coin Object
     * @return Coin Object
     */
    public Coin getCoin() {return coin;}
    /**
     * Getter for Asteroid Object.
     * @return Asteroid Object.
     */
    public Asteroid getAsteroid1() {return asteroid1;}

    /**
     * Getter for Laser Object.
     * @return Laser Object.
     */
    public Laser getLaser() {return laser;}
    /**
     * Getter for Background Object.
     * @return Background Object.
     */
    public Background getBackground() {return background;}
    /**
     * Getter for Flame Object
     * @return Flame Object
     */
    public Flame getFlame() {return flame;}
}
