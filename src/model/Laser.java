package model;

/**
 * This class is for creating a laser beam that could be shot by the Spaceship.
 */
public class Laser {
    /**
     * X- and Y-Position of the Laser beam.
     */
    private int xPos, yPos;
    /**
     * Speed of the Laser beam.
     */
    private int speed = 35;
    /**
     * Healthpoints of the Laser beam.
     */
    private int healthPoints = 1;
    /**
     * Checking if the Spaceship has shoot.
     */
    private boolean shoot = false;
    /**
     * Getter for the x Position.
     * @return x-Coordinate.
     */
    public int getxPos() {
        return xPos;
    }
    /**
     * Getter for the y Position.
     * @return y-Coordinate.
     */
    public int getyPos() {
        return yPos;
    }
    /**
     * Setter for the x Position.
     * @param xPos setts x-Coordinate.
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }
    /**
     * Setter for the y Position.
     * @param yPos setts y-Coordinate.
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }
    /**
     * Setter of the schoot boolean.
     * @param shoot setts if the Ship has shoot or not.
     */
    public void setShoot(boolean shoot) {
        this.shoot = shoot;
    }
    /**
     * Getter of the Healthpoints of the Laser beam.
     * @return Healthpoints.
     */
    public int getHealthPoints() {
        return healthPoints;
    }
    /**
     * Setter of the Healthpoints ot the Laser beam.
     * @param healthPoints for creating or destruction of an Asteroid.
     */
    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }
    /**
     * Getter of the Speed of the Laser beam.
     * @return The flying speed of the Laser.
     */
    public int getSpeed() {
        return speed;
    }
}
