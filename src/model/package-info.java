/**
 *
 * Inside these Package is the complete Logic of the Game,
 * to separate this from the drawing window is necessary for the implication of the MVC pattern
 */
package model;