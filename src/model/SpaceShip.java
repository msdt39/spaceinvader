package model;
/**
 * This Class is creating the Character the User can move.
 */
public class SpaceShip {
    /**
     * The X-Coordinate of the Spaceship.
     */
    private float xPos;
    /**
     * The Y-Coordinate of the Spaceship.
     */
    private float yPos;
    /**
     * The starting X-Coordinate of the Spaceship.
     */
    final float xStartPosition = 320;
    /**
     * The starting Y-Coordinate of the Spaceship.
     */
    final float yStartPosition = 750;
    /**
     * The health points of the Spaceship.
     */
    private int healthPoints;
    /**
     * The speed of the Spaceship for flying forward.
     */
    private float speedUp = 20;
    /**
     * The speed of the Spaceship for flying backward.
     */
    private float speedDown = 10;
    /**
     * The speed of the Spaceship for flying horizontal.
     */
    private float speedHorizontal = 17;
    /**
     * The Constuctor of the Spaceship Class.
     * @param xPos The X-Coordinate of the Ship.
     * @param yPos The Y-Coordinate of the Ship.
     * @param healthPoints The health points of the Ship.
     */
    public SpaceShip(float xPos, float yPos, int healthPoints) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.healthPoints = healthPoints;}
    /**
     * Method for getting the Position of the X-Coordinate.
     * @return the X-Coordinate.
     */
    public float getxPos() {
        return xPos;}
    /**
     * Method for getting the Position of the Y-Coordinate.
     * @return the Y-Coordinate.
     */
    public float getyPos() {
        return yPos;
    }
    /**
     * Method for setting the Position of the X-Coordinate.
     */
    public void setxPos(float xPos) {
        this.xPos = xPos;
    }
    /**
     * Method for setting the Position of the Y-Coordinate.
     */
    public void setyPos(float yPos) {
        this.yPos = yPos;
    }
    /**
     * Method for getting the health points of the Spaceship.
     * @return health points of the Ship.
     */
    public int getHealthPoints() {
        return healthPoints;
    }
    /**
     * Method for setting the health points of the Ship.
     */
    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    /**
     * Method for getting the Starting X-Coordinate of the Spaceship.
     * @return Starting X-Coordinate.
     */
    public float getxStartPosition() {
        return xStartPosition;
    }
    /**
     * Method for getting the Starting Y-Coordinate of the Spaceship.
     * @return Starting Y-Coordinate.
     */
    public float getyStartPosition() {
        return yStartPosition;
    }
    /**
     * Method for getting the speed for forward
     * @return The speed for flying forward
     */
    public float getSpeedUp() {return speedUp;}
    /**
     Method for getting the speed for backward
     * @return The speed for flying backward
     */
    public float getSpeedDown() {return speedDown;}

    /**
     * Method for Getting the speed for flying right or left
     * @return horizontal speed
     */
    public float getSpeedHorizontal() {return speedHorizontal;}
}
