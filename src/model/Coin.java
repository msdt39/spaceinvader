package model;

/**
 * This Class defines collectable named Coin for increasing the players score.
 */
public class Coin {
    /**
     * The 'health points' of the coin.
     */
    private int healthPoints = 1;
    /**
     * The X- and Y-Coordinate of the Coin.
     */
    private int xPos, yPos = -200;
    /**
     * The speed of the Coin.
     */
    private int speed = 8;
    /**
     * This method is in charge of moving the Coin from the Top to the Bottom of the Screen.
     * @return the actual value of the Y-Coordinate.
     */
    public int fly() {
        if(healthPoints > 0) {
            yPos += speed;
        } else {
            yPos = -200;
        }
        if(yPos > 1200) {
            yPos = -300;
        }
        return yPos;
    }
    /**
     * Callculate a new Position of the Coin on the X-Coordinate after resetting at the Top of the Screen.
     * @return The new X-Coordinate.
     */
    public int respawnX() {
        if(yPos > 1100) {
            xPos = 50 + (int) (Math.random() * (650 - 50));
        }
        return xPos;
    }
    /**
     * Method for setting the Position of the X-Coordinate.
     * @param xPos the X-Coordinate.
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }
    /**
     * Method for setting the Position of the Y-Coordinate.
     * @param yPos the Y-Coordinate.
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * Method for getting the Position of the X-Coordinate.
     * @return the X-Coordinate.
     */
    public int getxPos() {
        return xPos;
    }
    /**
     * Method for getting the Position of the Y-Coordinate.
     * @return the Y-Coordinate.
     */
    public int getyPos() {
        return yPos;
    }
}
