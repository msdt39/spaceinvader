package model;
/**
 * This Class is creating the Enemy Class the player have to dodge.
 */
public class Asteroid {
    /**
     * The health point of the Asteroid.
     */
    private int healthPoints = 1;
    /**
     * A Variable for checking if the Asteroid is destroyed or not.
     */
    private boolean isAlive = true;
    /**
     * The X- and Y-coordinate of the Asteroid, the yPos is the starting coordinate of the Asteroid.
     */
    private int xPos, yPos = -100;
    /**
     * The start speed of all Asteroids.
     */
    private int speed = 8;
    /**
     * This method is in charge of moving the Asteroid from the Top to the Bottom of the Screen.
     * @return the actual value of the Y-Coordinate.
     */
    public int fly() {
        if(healthPoints > 0) {
            yPos += speed;
        } else {
            yPos = -200;
        }
        if(yPos > 1200) {
            yPos = -200;
        }
        return yPos;
    }
    /**
     * Callculate a new Position of the Asteroid on the X-Coordinate after resetting at the Top of the Screen.
     * @return The new X-Coordinate.
     */
    public int respawnX() {
        if(yPos > 1100) {
            xPos = 50 + (int) (Math.random() * (650 - 50));
        }
        return xPos;}
    /**
     * Method for getting the Position of the X-Coordinate.
     * @return the X-Coordinate.
     */
    public int getxPos() {
        return xPos;
    }
    /**
     * Method for getting the Position of the Y-Coordinate.
     * @return the Y-Coordinate.
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * Method for setting the Position of the Y-Coordinate.
     * @param yPos the Y-Coordinate.
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }
    /**
     * Method for setting the Position of the X-Coordinate.
     * @param xPos the X-Coordinate.
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * Method for setting the Asteroid back to Alive
     * @param alive for drawing the Asteroid or not
     */
    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}
