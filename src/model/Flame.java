package model;

import java.util.Timer;
import java.util.TimerTask;
/**
 * This Class is creating a Flame for the Spaceship.
 */
public class Flame {
    /**
     * The Speed for the Timer.
     */
    private int speed = 100;
    /**
     * The Variable for Setting the Flame image.
     */
    private int flammeAnimation;
    /**
     * A temporal Variable for setting the flammeAnimation.
     */
    private int temp = 1;
    /**
     * A Timer object for creating a Timer.
     */
    private Timer timer;
    /**
     * Constructor of the Flame Class
     */
    public Flame() {
        var t2 = new Speed("Thread 2");
        t2.run();}
    /**
     * Sub Class for creating a Thread.
     */
    class Speed extends Thread {
        /**
         * Variable for giving the Thread a name.
         */
        private String name;
        /**
         * Constructor of the Sub Class Speed
         * @param name The Name of the Thread
         */
        public Speed(String name) {
            this.name = name;}
        /**
         * Default run() Method for running a Thread.
         */
        @Override
        public void run() {

                timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        if(temp == 0) {
                            flammeAnimation = 0;
                            temp++;
                        } else if(temp == 1) {
                            flammeAnimation = 1;
                            temp--;
                        }
                    }
                },0,speed);}}
    /**
     * Getter for the Variable flammeAnimation.
     * @return The Variable flameAnimmation.
     */
    public int getFlammeAnimation() {
        return flammeAnimation;}}
