package model;
/**
 * These are alle possiable directions the player can move the Spaceship.
 */
public enum Direction {
    /**
     * For flying forward.
     */
    UP,
    /**
     * For flying backward.
     */
    DOWN,
    /**
     * For flying to the Left side.
     */
    LEFT,
    /**
     * For flying to the Right side.
     */
    RIGHT,
    /**
     * For shooting a Laser beam
     */
    SHOOT}
