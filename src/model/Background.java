package model;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This Class is for "moving" the Background during the running game. It's for the feeling to fly forward.
 * The stradegy is, that there are two pictures, running after each other down the screen, for a realistic view,
 * both pictures are identic.
 */
public class Background {
    /**
     * This is the speed of the Timer moving the Background.
     */
    private int speed = 11;
    /**
     * The start Position from the first Background picture.
     */
    private int posY1 = 0;
    /**
     * The start Position from the second Background picture.
     */
    private int posY2 = -1000;
    /**
     * Creating a Timer for the movment of the Pictures.
     */
    private Timer timer;
    /**
     * The Constructer of the Class.
     */
    public Background() {
            backgroundMove();
    }

    /**
     * The Method that includes the Timer and let the pictures running down the screen.
     */
    public void backgroundMove() {
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if(posY1 < 990) {
                        posY1 += 2;
                    } else {
                        posY1 = -990;
                    }

                    if(posY2 < 990) {
                        posY2 += 2;
                    } else {
                        posY2 = -990;
                    }
                }
            },0, speed);}
    /**
     * Method for getting the position of the Y-Coordinate from the first picture.
     * @return the Y-Coordinate.
     */
    public int getPosY1() {
        return posY1;
    }
    /**
     * Method for getting the position of the X-Coordinate from the first picture.
     * @return the X-Coordinate.
     */
    public int getPosY2() {
        return posY2;
    }
}
